package com.sda.jeppesen.studentdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentUpdateRequest {
    private Long studentId;
    private String newName;
    private String newSurname;
    private Double newAverage;
}
