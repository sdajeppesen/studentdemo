package com.sda.jeppesen.studentdemo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// POJO - Plain Old Java Object
//          - rodzaj obiektu który posiada:
//                - konstruktor pusty
//                - gettery
//                - settery

// ObjectMapper - > obiekt/klasa która dokonuje konwersji
//                - obiekt POJO -> JSON  //read
//                - JSON -> obiekt POJO  //write

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {

    private String studentName;
    private String surname;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password; // chcemy ukryć to pole z bycia wyświetlanym

    private double average;

    private boolean pass;

    private ActivityStatus status;
}
