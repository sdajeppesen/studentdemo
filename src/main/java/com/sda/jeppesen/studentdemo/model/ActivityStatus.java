package com.sda.jeppesen.studentdemo.model;

public enum ActivityStatus {
    NIEOBECNY,           // 0
    OBECNY,              // 1
}
