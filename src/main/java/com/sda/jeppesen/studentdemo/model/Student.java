package com.sda.jeppesen.studentdemo.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity // hibernate
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // identity = identyfikator generuje się z bazy danych
    // sequence/table - id generuje hibernate = brak auto_increment
    // hibernate wstawi 1 i 2.
    private Long id;

    private String firstName;
    private String lastName;

    private String password; // chcemy ukryć to pole z bycia wyświetlanym

    private double average; // NIE MOŻE BYĆ NULL

    private boolean didPass;

    @Enumerated(value = EnumType.ORDINAL) // wartość liczbowa zgodna z kolejnością
    private ActivityStatus status;
}
