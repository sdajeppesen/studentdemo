package com.sda.jeppesen.studentdemo.model.mapper;

import com.sda.jeppesen.studentdemo.model.Student;
import com.sda.jeppesen.studentdemo.model.StudentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

// @Component == @Bean
//
@Mapper(componentModel = "spring")
public interface StudentMapper {
    @Mappings({
            @Mapping(target = "firstName", source = "studentName"),
            @Mapping(target = "lastName", source = "surname"),
//            @Mapping(target = "average", source = "average"),
            @Mapping(target = "didPass", source = "pass"),
            @Mapping(target = "password", source = "password"),
//            @Mapping(target = "id", ignore = true)
    })
    Student mapDtoToStudent(StudentDTO dto);


    @Mappings({
            @Mapping(source = "firstName", target = "studentName"),
            @Mapping(source = "lastName", target = "surname"),
            @Mapping(source = "average", target = "average"),
            @Mapping(source = "didPass", target = "pass"),
            @Mapping(source = "password", target = "password")
    })
    StudentDTO mapStudentToDTO(Student dto);
}
