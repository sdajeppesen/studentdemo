package com.sda.jeppesen.studentdemo.service;


import com.sda.jeppesen.studentdemo.model.Student;
import com.sda.jeppesen.studentdemo.model.StudentDTO;
import com.sda.jeppesen.studentdemo.model.StudentUpdateRequest;
import com.sda.jeppesen.studentdemo.model.mapper.StudentMapper;
import com.sda.jeppesen.studentdemo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor // @AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository; // jeśli nie ma autowired = null
    private final StudentMapper studentMapper;

    public boolean saveOrUpdate(StudentDTO studentdto) {
        if (studentdto.getSurname() == null || studentdto.getSurname().isEmpty()) {
            return false; // nie udało się zapisać
        }

        // DTO -> Domain (model bazy danych)
        // mapowanie
//        Student student = new Student();
//        student.setFirstName(studentdto.getStudentName());
//        student.setLastName(studentdto.getSurname());
//        student.setAverage(studentdto.getAverage());
//        student.setDidPass(studentdto.isPass());
//        student.setPassword(studentdto.getPassword());

        Student student = studentMapper.mapDtoToStudent(studentdto);

        studentRepository.save(student);

        return true;
    }

    public List<StudentDTO> listAllStudents() {
        // select * from students...
        return studentRepository.findAll()
                .stream()
                .map(studentMapper::mapStudentToDTO)
                .collect(Collectors.toList());
    }

    public List<StudentDTO> searchByLastName(String nazwisko) {
        return studentRepository.findAllByLastNameContaining(nazwisko)
                .stream()
                .map(studentMapper::mapStudentToDTO)
                .collect(Collectors.toList());
    }

    public List<StudentDTO> searchByAverage(Double avgFrom, Double avgTo) {
        return studentRepository.findAllByAverageBetween(avgFrom, avgTo)
                .stream()
                .map(studentMapper::mapStudentToDTO)
                .collect(Collectors.toList());
    }

    public boolean update(StudentUpdateRequest updateRequest) {
        // select * from students where id = ?
        Optional<Student> studentOptional = studentRepository.findById(updateRequest.getStudentId());
        if (studentOptional.isPresent()) { // jeśli udało się znaleźć rekord
            Student student = studentOptional.get();

            if (updateRequest.getNewName() != null) {
                student.setFirstName(updateRequest.getNewName());
            }
            if (updateRequest.getNewSurname() != null) {
                student.setLastName(updateRequest.getNewSurname());
            }
            if (updateRequest.getNewAverage() != null) {
                student.setAverage(updateRequest.getNewAverage());
            }

            studentRepository.save(student); // finalne zapisanie obiektu
            return true;
        }
        return false;
    }
}
