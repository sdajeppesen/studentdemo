package com.sda.jeppesen.studentdemo.controller;

import com.sda.jeppesen.studentdemo.model.Student;
import com.sda.jeppesen.studentdemo.model.StudentDTO;
import com.sda.jeppesen.studentdemo.model.StudentUpdateRequest;
import com.sda.jeppesen.studentdemo.repository.StudentRepository;
import com.sda.jeppesen.studentdemo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // wymiana informacji w postaci tekstu/danych JSON
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

//    @Autowired // jeśli zastosowany nad polem,
//          to po stworzeniu obiektu StudentController używa settera
//          do ustawienia instancji obiektu studentService
    private final StudentService studentService;

//    @Autowired // jeśli zastosowany nad konstruktorem to ustawia instajcję
//          podczas tworzenia obiektu StudentController
//    public StudentController(StudentService studentService) {
//        this.studentService = studentService;
//    }

    // localhost:8080/student/add
//    @PutMapping("/add")

    // localhost:8080/student
    @PutMapping()
    public ResponseEntity<Void> putIntoDatabase(@RequestBody StudentDTO obiekt){
        boolean result = studentService.saveOrUpdate(obiekt);

        if(result){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build(); // 400 = bad request = wina błędu zapytania = użytkownika
        }
    }

//    @GetMapping()
//    public List<Student> getAllFromDatabase(){
//        return studentService.listAllStudents();
//    }

    @GetMapping()
    public ResponseEntity<List<StudentDTO>> getAllFromDatabase(){
        return ResponseEntity.ok(studentService.listAllStudents());
    }

    // Get
    // request param - przykład: http://localhost:8080/student/search?nazwisko=WARTOSC
    // path variable - przykład: http://localhost:8080/student/search/WARTOSC
    @GetMapping("/search")
    public List<StudentDTO> getFromDatabaseByLastName(@RequestParam(name = "nazwisko") String nazwisko){
        return studentService.searchByLastName(nazwisko);
    }

    // Get
    // path variable - przykład: http://localhost:8080/student/search/1.0/2.0
    @GetMapping("/search/{avg_from}/{avg_to}")
    public List<StudentDTO> getFromDatabaseByAverage(@PathVariable(name = "avg_from") Double avgFrom,
                                                     @PathVariable(name = "avg_to") Double avgTo){
        return studentService.searchByAverage(avgFrom, avgTo);
    }

    @PostMapping()
    public ResponseEntity<Void> updateStudent(@RequestBody StudentUpdateRequest updateRequest){
        boolean result = studentService.update(updateRequest);

        if(result){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build(); // 400 = bad request = wina błędu zapytania = użytkownika
        }
    }
}
