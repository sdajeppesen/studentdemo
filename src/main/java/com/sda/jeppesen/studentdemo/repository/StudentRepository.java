package com.sda.jeppesen.studentdemo.repository;

import com.sda.jeppesen.studentdemo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pozwala wywołać zapytania bazodanowe.
 *
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    // cel - chcę znaleźć wszystkich studentów o nazwisku "XYZ"
    // select * from student where nazwisko = "..."
    List<Student> findAllByLastName(String lastName); // metoda zostanie wygenerowana przez spring

    // cel - chcę znaleźć wszystkich studentów o nazwisku "XYZ"
    // select * from student where nazwisko like "%...%"
    List<Student> findAllByLastNameContaining(String lastName); // metoda zostanie wygenerowana przez spring

    List<Student> findAllByAverageBetween(Double from, Double to);
}
